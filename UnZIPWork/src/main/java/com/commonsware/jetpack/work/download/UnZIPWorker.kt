/*
  Copyright (c) 2017-2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.work.download

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.commonsware.cwac.security.ZipUtils
import java.io.File

class UnZIPWorker(context: Context, workerParams: WorkerParameters) :
  Worker(context, workerParams) {

  override fun doWork(): Result {
    val downloadedFile = File(inputData.getString(KEY_ZIPFILE)!!)
    val dir = applicationContext.cacheDir
    val resultDirData = inputData.getString(KEY_RESULTDIR)
    val resultDir = File(dir, resultDirData ?: "results")

    try {
      ZipUtils.unzip(downloadedFile, resultDir, 2048, 1024 * 1024 * 16)
      downloadedFile.delete()
    } catch (e: Exception) {
      Log.e(javaClass.simpleName, "Exception unZIPing file", e)

      return Result.failure()
    }

    return Result.success()
  }

  companion object {
    const val KEY_ZIPFILE = "zipFile"
    const val KEY_RESULTDIR = "resultDir"
  }
}
