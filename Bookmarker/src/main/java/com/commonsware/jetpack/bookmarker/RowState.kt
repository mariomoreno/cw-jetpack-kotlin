/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker

import android.text.Spanned
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DiffUtil

class RowState(model: BookmarkModel) {
  val title: Spanned =
    HtmlCompat.fromHtml(model.title ?: "", HtmlCompat.FROM_HTML_MODE_COMPACT)
  val iconUrl = model.iconUrl
  val pageUrl = model.pageUrl

  companion object {
    val DIFFER: DiffUtil.ItemCallback<RowState> =
      object : DiffUtil.ItemCallback<RowState>() {
        override fun areItemsTheSame(
          oldItem: RowState,
          newItem: RowState
        ): Boolean {
          return oldItem === newItem
        }

        override fun areContentsTheSame(
          oldItem: RowState,
          newItem: RowState
        ): Boolean {
          return oldItem.title.toString() == newItem.title.toString()
        }
      }
  }
}
